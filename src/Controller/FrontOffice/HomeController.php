<?php

namespace App\Controller\FrontOffice;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Controller used to homepage contents in the public part of the site.
 */
class HomeController extends AbstractController
{
    /**
     * @return Response
     */
    public function index()
    {
        return $this->render('frontoffice/home/index.html.twig');
    }
}